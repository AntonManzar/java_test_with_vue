package ru.pride.testWithVue.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pride.testWithVue.models.SportProgram;

@Repository
public interface SportProgramRepository extends JpaRepository<SportProgram, Integer> {
}
