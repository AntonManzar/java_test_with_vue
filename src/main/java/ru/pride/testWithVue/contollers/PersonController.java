package ru.pride.testWithVue.contollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.pride.testWithVue.models.Person;
import ru.pride.testWithVue.services.PersonService;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {
    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @CrossOrigin
    @GetMapping("/")
    public List<Person> getListPerson() {
        return personService.getListPerson();
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public Person getPerson(@PathVariable("id") int id) {
        return personService.getPersonById(id);
    }

    @CrossOrigin
    @PostMapping("/")
    public Person savePerson(@RequestBody Person person) {
        personService.savePerson(person);
        return person;
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    public Person savePerson(@PathVariable("id") int id) {
        Person person = personService.getPersonById(id);
        personService.deletePersonById(id);
        return person;
    }
}
