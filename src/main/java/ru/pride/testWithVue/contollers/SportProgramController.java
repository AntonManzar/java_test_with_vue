package ru.pride.testWithVue.contollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.pride.testWithVue.dto.SportProgramDTO;
import ru.pride.testWithVue.models.SportProgram;
import ru.pride.testWithVue.services.SportProgramService;

import java.util.List;

@RestController
@RequestMapping("/sport")
public class SportProgramController {

    private final SportProgramService sportProgramService;

    @Autowired
    public SportProgramController(SportProgramService sportProgramService) {
        this.sportProgramService = sportProgramService;
    }

    @CrossOrigin
    @GetMapping("/")
    public List<SportProgramDTO> getListSportProgram() {
        List<SportProgramDTO> list = sportProgramService.getListSportProgramDTO();
        return list;
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public SportProgramDTO getPerson(@PathVariable("id") int id) {
        return sportProgramService.getSportProgramDTOById(id);
    }
}
