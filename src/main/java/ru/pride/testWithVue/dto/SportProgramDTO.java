package ru.pride.testWithVue.dto;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.pride.testWithVue.models.Exercise;
import ru.pride.testWithVue.models.SportProgram;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SportProgramDTO {

    private int id;

    private String nameWorkout;

    private List<ExerciseDTO> exercisesDTO;

    public static SportProgramDTO convertToDTO(SportProgram sportProgram) {
        if (sportProgram == null) {
            return null;
        }
        SportProgramDTO sportProgramDTO = new SportProgramDTO();
        sportProgramDTO.setId(sportProgram.getId());
        sportProgramDTO.setNameWorkout(sportProgram.getNameWorkout());
        sportProgramDTO.setExercisesDTO(ExerciseDTO.convertToListDTO(sportProgram.getExercises()));
        return sportProgramDTO;
    }

    public static List<SportProgramDTO> convertToListDTO(List<SportProgram> sportProgramList) {
        List<SportProgramDTO> sportProgramDTOList = new ArrayList<>();
        for (SportProgram sportProgram : sportProgramList) {
            sportProgramDTOList.add(convertToDTO(sportProgram));
        }
        return sportProgramDTOList;
    }
}
