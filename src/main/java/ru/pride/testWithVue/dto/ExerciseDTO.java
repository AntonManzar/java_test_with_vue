package ru.pride.testWithVue.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.pride.testWithVue.models.Exercise;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExerciseDTO {

    private int id;

    private String name;

    private String text;

    private String count;

    private String gifRef;

    private int sportProgramId;

    public static ExerciseDTO convertToDTO(Exercise exercise) {
        ExerciseDTO exerciseDTO = new ExerciseDTO();
        exerciseDTO.setId(exercise.getId());
        exerciseDTO.setName(exercise.getName());
        exerciseDTO.setText(exercise.getText());
        exerciseDTO.setCount(exercise.getCount());
        exerciseDTO.setGifRef(exercise.getGifRef());
        exerciseDTO.setSportProgramId(exercise.getSportProgram().getId());
        return exerciseDTO;
    }

    public static List<ExerciseDTO> convertToListDTO(List<Exercise> exerciseList) {
        List<ExerciseDTO> exerciseDTOList = new ArrayList<>();
        for (Exercise exercise : exerciseList) {
            exerciseDTOList.add(convertToDTO(exercise));
        }
        return exerciseDTOList;
    }
}
