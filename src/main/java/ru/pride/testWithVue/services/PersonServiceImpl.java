package ru.pride.testWithVue.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pride.testWithVue.models.Person;
import ru.pride.testWithVue.repositories.PersonRepository;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public List<Person> getListPerson() {
        return personRepository.findAll();
    }

    @Override
    public Person getPersonById(int personId) {
        return personRepository.findById(personId).orElse(null);
    }

    @Override
    @Transactional
    public void savePerson(Person person) {
        personRepository.save(person);
    }

    @Override
    @Transactional
    public void deletePersonById(int id) {
        personRepository.deleteById(id);
    }
}
