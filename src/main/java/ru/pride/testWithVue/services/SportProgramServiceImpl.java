package ru.pride.testWithVue.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pride.testWithVue.dto.SportProgramDTO;
import ru.pride.testWithVue.models.SportProgram;
import ru.pride.testWithVue.repositories.SportProgramRepository;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class SportProgramServiceImpl implements SportProgramService {

    private final SportProgramRepository sportProgramRepository;

    @Autowired
    public SportProgramServiceImpl(SportProgramRepository sportProgramRepository) {
        this.sportProgramRepository = sportProgramRepository;
    }

    @Override
    public List<SportProgramDTO> getListSportProgramDTO() {
        List<SportProgram> sportProgramList = sportProgramRepository.findAll();
        return SportProgramDTO.convertToListDTO(sportProgramList);
    }

    @Override
    public SportProgramDTO getSportProgramDTOById(int sportProgramId) {
        SportProgram sportProgram = sportProgramRepository.findById(sportProgramId).orElse(null);
        return SportProgramDTO.convertToDTO(sportProgram);
    }
}
