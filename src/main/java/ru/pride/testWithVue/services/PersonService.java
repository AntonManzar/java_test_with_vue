package ru.pride.testWithVue.services;

import ru.pride.testWithVue.models.Person;
import java.util.List;

public interface PersonService {
    List<Person> getListPerson();
    Person getPersonById(int personId);
    void savePerson(Person person);
    void deletePersonById(int id);
}
