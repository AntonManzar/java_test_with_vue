package ru.pride.testWithVue.services;

import ru.pride.testWithVue.dto.SportProgramDTO;

import java.util.List;

public interface SportProgramService {
    List<SportProgramDTO> getListSportProgramDTO();
    SportProgramDTO getSportProgramDTOById(int sportProgramId);
}
