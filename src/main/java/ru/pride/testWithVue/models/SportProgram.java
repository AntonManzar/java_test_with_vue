package ru.pride.testWithVue.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "sport_program")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SportProgram {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @NotEmpty(message = "Поле для имени не может быть пустым")
    @Column(name = "name_workout")
    private String nameWorkout;

    @OneToMany(mappedBy = "sportProgram", cascade = CascadeType.PERSIST)
    private List<Exercise> exercises;

    public void addExercise(Exercise exercise) {
        exercises.add(exercise);
        exercise.setSportProgram(this);
    }

    public void removeExercise(Exercise exercise) {
        exercises.remove(exercise);
        exercise.setSportProgram(null);
    }
}
