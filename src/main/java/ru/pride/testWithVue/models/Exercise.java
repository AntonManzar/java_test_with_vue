package ru.pride.testWithVue.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.type.descriptor.jdbc.VarcharJdbcType;

@Entity
@Table(name = "exercise")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Exercise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @NotEmpty(message = "Поле для имени не может быть пустым")
    @Column(name = "name")
    private String name;

    @Column(name = "text")
    private String text;

    @Column(name = "count")
    private String count;

    @Column(name = "gifRef")
    private String gifRef;

    @ManyToOne(fetch = FetchType.LAZY)
    private SportProgram sportProgram;
}
